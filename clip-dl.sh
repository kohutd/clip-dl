#!/bin/bash

if [[ ! $(type dunstify) ]]; then
	notify-send --category=error "dunstify is not installed" "please install it"
	exit
fi

function notify() {
	url=$1
	serv=$2
	can_video=$3
	serv_icon=$4

	if [[ -z $can_video ]]; then
		can_video=false
	fi
	if [[ -z $serv_icon ]]; then
		serv_icon="sound"
	fi

	if [ "$can_video" = true ]; then
		type=$(dunstify --icon="$serv_icon" "$serv link detected" -A mp3,MP3 -A mp4,MP4)
	else
		type=$(dunstify --icon="$serv_icon" "$serv link detected" -A mp3,MP3)
	fi

	if [[ "$type" != 2 ]]; then
		./getm.sh $url $type
	fi
}


last=$(xclip -out -selection)

while true; do
	last_tmp=$(xclip -out -selection clipboard)

	if [[ $last != $last_tmp ]]; then

		if [[ "$last_tmp" = *"youtube.com/watch"* ]]; then
			notify $last_tmp "YouTube" true "youtube" &
		elif [[ "$last_tmp" = *"soundcloud.com"* ]]; then
			notify $last_tmp "SoundCloud" false "soundcloud" &
		elif [[ "$last_tmp" = *"pornhub.com"* ]] || [[ "$last_tmp" = *"youporn.com"* ]]; then
			notify $last_tmp "Pron" true "girl" &
		fi

	fi

	last=$(xclip -out -selection clipboard)
	sleep .1
done